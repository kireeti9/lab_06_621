from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/signup_form')
def signup_form():
    return render_template('signup.html')

@app.route('/password_submit')
def password_submit():
     password = request.args.get('password')
     errors = []
     if not any(char.islower() for char in password):
         errors.append('Password should have at least one lowercase letter')

     if not any(char.isupper() for char in password):
         errors.append('Password should have at least one uppercase letter')

     if not (password[-1].isdigit()):
         errors.append('Password should end with a number')

     return render_template('password_submit.html',errors=errors)

@app.errorhandler(404)
def page_not_found(e):
     return render_template('404.html'),404

if __name__=='__main__':
     app.run(debug=True)
