from flask import Flask

app = Flask(__name__)


@app.route('/users/<username>')
def userprofile(username):
    uname = format(username)

    if uname[-1] == 'y':
        stripped_uname = uname[:-1]
        return " <h3> Your real name is : " + format(username) + " and your latin name is : " + stripped_uname + "iful"

    if uname[-1] != 'y':
        return "<h3> Your real name is : " + format(username) + " and your latin name is : " + format(username) + "y"

if __name__ == '__main__':
    app.run(debug=True)